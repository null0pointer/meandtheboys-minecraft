import os

def getPathSize(d):
    if os.path.isfile(d):
        return os.path.getsize(d)
    if os.path.isdir(d):
        return sum(getPathSize(os.path.join(d, sd)) for sd in os.listdir(d))
    # Ignore symlinks and stuff
    return 0
