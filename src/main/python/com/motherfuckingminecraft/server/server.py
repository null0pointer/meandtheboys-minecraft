import http.server
import utils
import json

PORT = 80

class RequestHandler(http.server.BaseHTTPRequestHandler):

    def do_GET(self):
        worldsize = utils.getPathSize('/opt/minecraft/world/')
        self.send_response(200)
        self.end_headers()
        body = {
            'com.motherfuckingminecraft.server': {
                'web': {
                    'minecraft': {
                        'world': {
                            'size': {
                                'value': f'{worldsize:,d}',
                                'unit': 'bytes'
                            }
                        }
                    }
                }
            },
            'debug': 'checking that the cron deployment actually works... and again??...'
        }
        body = json.dumps(body, indent=2)
        self.wfile.write(body.encode())

with http.server.HTTPServer(("", PORT), RequestHandler) as httpd:
    print("serving at port", PORT)
    httpd.serve_forever()
