#!/usr/bin/env bash

# USAGE:
# $ docker login registry.gitlab.com
# $ bash scripts/build.sh
#
# It is recommended to create a personal access token for docker login
# https://gitlab.com/help/user/profile/personal_access_tokens
#
# A way to automate it would be:
# $ cat gitlab_access_token_for_docker | docker login --username ch41rmn --password-stdin

set -x

export COMPOSE_FILE=src/main/resources/build/docker-compose.yaml

docker-compose build
docker-compose push
