#!/usr/bin/env bash

# USAGE:
# $ bash ci/ci.sh <your-gitlab-username>
#
# NOTES:
# - put your gitlab access token into ci/gitlab_access_token_for_docker.txt
# - add this to crontab

set -x

GITLAB_USER=${1:-null0pointer}

git fetch

LOCAL=$(git rev-parse @)
REMOTE=$(git rev-parse @{u})

if [ $LOCAL = $REMOTE ]; then
    echo "Up-to-date"
else
    cat ci/gitlab_access_token_for_docker.txt | docker login --username ${GITLAB_USER} --password-stdin registry.gitlab.com

    git pull
    bash ci/build.sh
    bash ci/deploy.sh
fi
